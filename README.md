## Install & Run the Project

-1. Download & install the latest .NET Core 3.1.

-2 https://dotnet.microsoft.com/download

-3 Open Visual Studio 2019.

-4 Click on the 'Clone Repository'.

-5 Copy & Paste the URL of the project.

-6 Open the project.

----

## License & Copyrights.

© Prathamesh U Naik, Conestoga College.

----



